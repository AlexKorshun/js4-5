"use strict";

function createNewUser(s) {
    let newUser = {
        _firstName: '',
        _lastName: '',
        _birthday: new Date(),
        getAge() {
            let today = new Date();
            let age = today.getFullYear() - this._birthday.getFullYear();
            let month = this._birthday.getMonth();
            if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < this._birthday.getDate())) {
                age--;
            }
            return age;
        },
        getPassword() {
            return this._firstName.charAt(0).toUpperCase() + this._lastName.toLowerCase() + this._birthday.getFullYear().toString();
        },
        getLogin() {
            return this._firstName.charAt(0).toLowerCase() + this._lastName.toLowerCase();
        },
        getNormalDate() {
            let monthStr = (Number(this._birthday.getMonth()) + 1).toString();
            let month = monthStr.length === 1 ? '0' + monthStr : monthStr;
            return this._birthday.getDate() + '.' + month + '.' + this._birthday.getFullYear();
        },
        set birthday(val) {
            this._birthday = val;
        },
        get birthday() {
            return this._birthday;
        },
        set firstName(val) {
            this._firstName = val;
        },
        get firstName() {
            return this._firstName;
        },
        set lastName(val) {
            this._lastName = val;
        },
        get lastName() {
            return this._lastName;
        }

    };

    newUser.firstName = prompt('Enter your Name please');
    newUser.lastName = prompt('Enter your LastName please');
    let enterDate = prompt('Enter your birthday (dd.mm.yyyy)')
    let pattern = /^(\d{1,2})\.(\d{1,2})\.(\d{4})$/;
    let arrayDate = enterDate.match(pattern);
    try {
        newUser.birthday = new Date(arrayDate[3], arrayDate[2] - 1, arrayDate[1]);
    }catch (e){
        alert('You birthday is wrong! I\'ll set today')
    }
    return newUser;
}

let user = createNewUser();
console.log(user);
alert('You enter - '+user.firstName + ' ' + user.lastName + ', ' + user.getNormalDate() + ' \n'
    + 'Your login is \'' + user.getLogin() + '\' , password is \'' + user.getPassword() + '\' , age is \'' + user.getAge() + '\'');



